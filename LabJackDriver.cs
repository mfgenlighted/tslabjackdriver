﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LabJack.LabJackUD;

namespace LabJackDriver
{

    public struct channelDefsS
    {
        public string name;
        public string type;
        public int channel;
        public int range;
        public int gain;
        public double lastreading;
    }


   public class LabJack
    {
        private U3 u3 = null;                               // instance for the LabJack

         private class channelDefs
        {
            public string name;
            public string type;
            public int channel;
            public int range;
            public int gain;
            public double lastreading;
        }

        private List<channelDefs> channels = new List<channelDefs>();


        //---------------------------------------
        //--------- interface functions
        //---------------------------------------


        /// <summary>
        /// Connect to the instrument. Channel List needs to be setup before calling.
        /// </summary>
        /// <returns></returns>
        public void Connect(ref channelDefsS[] channelDefs, out int errorCode, out string errorMsg, out bool errorOccured)
        {
            LJUD.IO ioType = 0;
            LJUD.CHANNEL channel = 0;
            double dblValue = 0;
            int dummyInt = 0;
            double dummyDouble = 0;
            bool finished = false;
            channelDefs xfer = new channelDefs();

            for (int i = 0; i < channelDefs.Length; i++)
            {
                xfer = new channelDefs();
                xfer.name = channelDefs[i].name;
                xfer.type = channelDefs[i].type;
                xfer.channel = channelDefs[i].channel;
                xfer.range = channelDefs[i].range;
                xfer.gain = channelDefs[i].gain;
                xfer.lastreading = channelDefs[i].lastreading;
                channels.Add(xfer);
            }

            errorCode = 0;
            errorMsg = string.Empty;
            errorOccured = false;

            try
            {
                u3 = new U3(LJUD.CONNECTION.USB, "0", true); // Connection through USB
            }
            catch (Exception ex)
            {
                errorMsg = "Error connecting to the LabJack: " + ex.Message;
                errorCode = -1111;
                errorOccured = true;
                return;
            }
            // now have to figure out the channel config.

            //Start by using the pin_configuration_reset IOType so that all
            //pin assignments are in the factory default condition.
            LJUD.ePut(u3.ljhandle, LJUD.IO.PIN_CONFIGURATION_RESET, 0, 0, 0);

            // now loop thru and set the correct pin defs
            foreach (var item in channels)
            {
                if ((item.type.ToUpper() == "ACINPUT") | (item.type.ToUpper() == "DCINPUT") | (item.type.ToUpper() == "TEMPERATUREC")
                     | (item.type.ToUpper() == "TEMPERATUREF") | (item.type.ToUpper() == "TEMPERATUREK"))
                {
                    LJUD.AddRequest(u3.ljhandle, LJUD.IO.PUT_ANALOG_ENABLE_BIT, item.channel, 1, 0, 0);
                }
                if ((item.type.ToUpper() == "DINPUT") | (item.type == "DOUTPUT") | (item.type == "CONTACT"))
                {
                    LJUD.AddRequest(u3.ljhandle, LJUD.IO.PUT_DIGITAL_BIT, item.channel, 0, 0, 0);
                }
            }
            LJUD.GoOne(u3.ljhandle);
            // clear out the results from all the adds
            try { LJUD.GetFirstResult(u3.ljhandle, ref ioType, ref channel, ref dblValue, ref dummyInt, ref dummyDouble); }
            catch (Exception e)
            {
                errorMsg = "Error setting up the LabJack: " + e.Message;
                errorCode = -1111;
                errorOccured = true;
                return;
            }

            while (!finished)
            {
                try { LJUD.GetNextResult(u3.ljhandle, ref ioType, ref channel, ref dblValue, ref dummyInt, ref dummyDouble); }
                catch (LabJackUDException e)
                {
                    // If we get an error, report it.  If the error is NO_MORE_DATA_AVAILABLE we are done
                    if (e.LJUDError == UE9.LJUDERROR.NO_MORE_DATA_AVAILABLE)
                        finished = true;
                    else
                    {
                        errorMsg = "Error setting up the LabJack: " + e.Message;
                        errorCode = -1111;
                        errorOccured = true;
                        return;
                    }
                }
            }

            return;
        }

        public void Disconnect()
        {
            u3 = null;
        }

        public double ReadChannel(string channelName, out int errorCode, out string errorMsg, out bool errorOccured)
        {
            channelDefsS[] channelData = new channelDefsS[1];


            channelData[0].name = channelName;
            ReadListOfChannels(ref channelData, out  errorCode, out  errorMsg, out  errorOccured); // do not have to do errorcheck. LastErrorxxx set by ReadListOfChannels
            return channelData[0].lastreading;

        }

        /// <summary>
        /// Measure the inputs defined in channelNames. Only the name is used. The value is passed back in the
        /// same list.
        /// </summary>
        /// <param name="channelNames"></param>
        public void ReadListOfChannels(ref channelDefsS[] channelsData, out int errorCode, out string errorMsg, out bool errorOccured)
        {
            bool bresult = false;
            channelDefs channelData;
            string cname = string.Empty;

            if (u3 == null)          // if the labjack is not set up
            {
                errorMsg = "Labjack has not been set up.";
                errorCode = -1111;
                errorOccured = true;
                return ;
            }

            for (int i = 0; i < channelsData.Length; i++)
            {
                cname = channelsData[i].name;
                if ((channelData = channels.Find(x => x.name == cname)) == null)       // if the name was not found
                {
                    errorMsg = "Channel " + cname + " is not defined.";
                    errorCode = -1111;
                    errorOccured = true;
                    return;
                }
                try
                {
                    if ((channelData.type == "ACINPUT") | (channelData.type == "DCINPUT") | (channelData.type == "TEMPERATUREC")
                        | (channelData.type == "TEMPERATUREK") | (channelData.type == "TEMPERATUREF"))
                    {
                        channelsData[i].lastreading = ReadAnalogChannel(channelData.name, out errorCode, out errorMsg, out errorOccured);
                        if (errorOccured)     // errorOccured set by ReadAnalogChannel
                        {
                            return;
                        }
                    }
                    else if (channelData.type == "DINPUT")
                    {
                        bresult = ReadDigitalChannel(channelData.name, out errorCode, out errorMsg, out errorOccured);
                        if (errorOccured)
                        {
                            return;
                        }
                        channelsData[i].lastreading = bresult ? 1 : 0;
                    }
                    else
                    {
                        errorCode = -1111;
                        errorOccured = true;
                        errorMsg = "Channel " + channelsData[i].name + " is not a input channel.";
                        return;
                    }
                }
                catch (LabJackUDException ex)
                {
                    errorMsg = ex.Message + ex.LJUDError.ToString();
                    errorCode = -1111;
                    errorOccured = true;
                    return;
                }
            }

            // if it gets to here, there was no errors
            errorCode = 0;
            errorMsg = string.Empty;
            errorOccured = false;

            return;
        }

        public void SetChannel(string channelName, double value, out int errorCode, out string errorMsg, out bool errorOccured)
        {
            string cname = string.Empty;


            channelDefs channelData;

            if (u3 == null)          // if the labjack is not set up
            {
                errorMsg = "Labjack has not been set up.";
                errorCode = -1111;
                errorOccured = true;
                return;
            }

            if ((channelData = channels.Find(x => x.name == channelName)) == null)       // if the name was not found
            {
                errorMsg = "Channel " + channelName + " is not defined.";
                errorCode = -1111;
                errorOccured = true;
                return;
            }

            if ((channelData.type == "DOUPUT") | (channelData.type == "CONTACT"))
            {
                SetDigitalChannel(channelName, (int)value, out errorCode, out errorMsg, out errorOccured);
            }
            else if (channelData.type == "DAC")
            {
                SetDAC(channelName, value, out errorCode, out errorMsg, out errorOccured);
            }
            else
            {
                errorMsg = "Channel " + channelName + " is not a output";
                errorCode = -1111;
                errorOccured = true;
                return;
            }

        }

        //-------------------------------------------------
        //  LabJack support for interface functions
        //------------------------------------------------

        private double ReadAnalogChannel(string channelName, out int errorCode, out string errorMsg, out bool errorOccured)
        {
            double readvalue = 0;
            channelDefs channelToRead;

            errorCode = 0;
            errorMsg = string.Empty;
            errorOccured = false;

            if (u3 == null)          // if the labjack is not set up
            {
                errorMsg = "Labjack has not been set up.";
                errorCode = -1111;
                errorOccured = true;
                return 0;
            }

            if ((channelToRead = channels.Find(x => x.name == channelName)) == null)       // if the name was not found
            {
                errorMsg = "Channel " + channelName + " is not defined.";
                errorCode = -1111;
                errorOccured = true;
                return 0;
            }
            if ((channelToRead.type != "ACINPUT") & (channelToRead.type != "DCINPUT") &
                        (channelToRead.type != "TEMPERATUREC") & (channelToRead.type != "TEMPERATUREF")
                        & (channelToRead.type != "TEMPERATUREK"))
            {
                errorMsg = "Channel " + channelName + " is not a analog input.";
                errorCode = -1111;
                errorOccured = true;
                return 0;
            }
            try
            {
                LJUD.eGet(u3.ljhandle, LJUD.IO.GET_AIN, channelToRead.channel, ref readvalue, 0);
            }
            catch (LabJackUDException ex)
            {
                errorMsg = ex.Message + ex.LJUDError.ToString();
                errorCode = -1111;
                errorOccured = true;
                return 0;
            }

            readvalue = readvalue * channelToRead.gain;
            return readvalue;
        }

        /// <summary>
        /// Read a digital input. Check LastFunctionStatus to make sure successfull.
        /// </summary>
        /// <param name="channelName">Text name of channel to read</param>
        /// <returns></returns>
        private bool ReadDigitalChannel(string channelName, out int errorCode, out string errorMsg, out bool errorOccured)
        {
            int readvalue = 0;
            channelDefs channelToRead;

            errorCode = 0;
            errorMsg = string.Empty;
            errorOccured = false;


            if (u3 == null)          // if the labjack is not set up
            {
                errorMsg = "Labjack has not been set up.";
                errorCode = -1111;
                errorOccured = true;
                return false;
            }

            if ((channelToRead = channels.Find(x => x.name == channelName)) == null)       // if the name was not found
            {
                errorMsg = "Channel " + channelName + " is not defined.";
                errorCode = -1111;
                errorOccured = true;
                return false;
            }
            if (channelToRead.type != "DINPUT")
            {
                errorMsg = "Channel " + channelName + " is not a digital input.";
                errorCode = -1111;
                errorOccured = true;
                return false;
            }
            try
            {
                LJUD.eDI(u3.ljhandle, channelToRead.channel, ref readvalue);
            }
            catch (LabJackUDException ex)
            {
                errorMsg = ex.Message + ex.LJUDError.ToString();
                errorCode = -1111;
                errorOccured = true;
                return false;
            }
            if (readvalue == 0)     // weak pull up shorted when cover is closed
                return false;
            else
                return true;
        }

        private void SetDigitalChannel(string channelName, int value, out int errorCode, out string errorMsg, out bool errorOccured)
        {
            channelDefs channelToRead;

            errorCode = 0;
            errorMsg = string.Empty;
            errorOccured = false;

            if (u3 == null)          // if the labjack is not set up
            {
                errorMsg = "Labjack has not been set up.";
                errorCode = -1111;
                errorOccured = true;
                return;
            }

            if ((channelToRead = channels.Find(x => x.name == channelName)) == null)       // if the name was not found
            {
                errorMsg = "Channel " + channelName + " is not defined.";
                errorCode = -1111;
                errorOccured = true;
                return;
            }
            if (channelToRead.type != "DOUTPUT")
            {
                errorMsg = "Channel " + channelName + " is not a digital output.";
                errorCode = -1111;
                errorOccured = true;
                return;
            }
            try
            {
                LJUD.eDO(u3.ljhandle, channelToRead.channel, value);
            }
            catch (LabJackUDException ex)
            {
                errorMsg = ex.Message + ex.LJUDError.ToString();
                errorCode = -1111;
                errorOccured = true;
                return;
            }
        }

        void SetDAC(string channelName, double value, out int errorCode, out string errorMsg, out bool errorOccured)
        {
            channelDefs channelToRead;

            errorCode = 0;
            errorMsg = string.Empty;
            errorOccured = false;


            if (u3 == null)          // if the labjack is not set up
            {
                errorMsg = "Labjack has not been set up.";
                errorCode = -1111;
                errorOccured = true;
                return;
            }


            if ((channelToRead = channels.Find(x => x.name == channelName)) == null)       // if the name was not found
            {
                errorMsg = "Channel " + channelName + " is not defined.";
                errorCode = -1111;
                errorOccured = true;
                return;
            }
            if (channelToRead.type != "DAC")
            {
                errorMsg = "Channel " + channelName + " is not a DAC.";
                errorCode = -1111;
                errorOccured = true;
                return;
            }
            try
            {
                LJUD.eDAC(u3.ljhandle, channelToRead.channel, value, 0, 0, 0);
            }
            catch (LabJackUDException ex)
            {
                errorMsg = ex.Message + ex.LJUDError.ToString();
                errorCode = -1111;
                errorOccured = true;
                return;
            }
        }

    }
}
